

var config = {}



let idTimeout= 0
let rechercheEnCours = false
let pageDateOuverte = false
let idTabsContentScript = []


console.log("lancement du script d'arrère plan")

function traiterCreneaux(creneauxJson){
	
	function isDateValide(cre){ //déterminer si le créneau correspond aux disponibilités (config.date)
		if (config.dispos){
			return true
		}
		try {
			return config.date[cre.dateDebut.slice(0,4)][cre.dateDebut.slice(5,7)].includes(cre.dateDebut.slice(8,10))
		}
		catch(e){
			return false
		}
	}
	
	function choisirDate(creneaux){ //traiter la réponse JSON, pour en extraire la liste des id des places qui conviennent
		let creneauxCentre = creneaux.filter( (elementCentre) => {return (config.centre ? elementCentre.centre.nom == config.centre : true)})[0].creneaux // le JSON est une liste dont chaque élément correspondàun centre, et contient un objet centre et une liste créneaux, on récupére la liste créneaux
		//console.log(creneauxCentre)
		return creneauxCentre.filter( (cre) => {return (cre.categoriePermis == "B")} )
			.filter(isDateValide)
			.map( (cre) => {return cre.id} )
	}
	
	console.log("Réception des créneaux disponibles:")
	console.log(creneauxJson)
	
	return choisirDate(creneauxJson)
	
};


//quand une requête est faite pour chercher les créneaux disponibles dans le département... (quand on clique sur le bouton "Recherche")
function listener(e){
	function recharger(idOnglet){																			//recliquer sur le bouton après 5 secondes
		clearTimeout(idTimeout)
		idTimeout = setTimeout(()=>{						
			browser.tabs.sendMessage(idOnglet, {															//on demande au content script de cliquer sur le bouton "Recherche"
				action: "CHERCHER"
			})
		}, config.delai)																	// sauvegarder l'id du timeout dans la listeIdTimeout
	}
	
	
	let filter = browser.webRequest.filterResponseData(e.requestId);										//intercepter la réponse à la requête
	
	filter.ondata = ((event) => {
		let response;
		if (config.isTest){																					//si on est en train de faire un test, on crée une réponse à partir de données qu'on a déjà, avec des places disponibles, ce sera ensuite envoyé à la page avec filter.write(response)
			console.log("données créneaux modifiées")
			response = new TextEncoder().encode(config.testData)
		}else{
			//console.log("données créneaux pas modifiées")
			response = event.data																			//sinon, on garde les données interceptées
		}
		filter.write(response)																				//on renvoie la réponse (modifiées ou non)
		filter.disconnect()
		if(response.byteLength === 2){																		//si la réponse est une liste vide [], on recharge simplement la page (pas de places dispo)
			console.log("pas de places disponibles, la recherche va être relancée...")
			recharger(e.tabId)
			return(null)
		}	
		try {																								//sinon on récupère le json de la réponse
			parsedJson = JSON.parse(new TextDecoder().decode(response));
		} catch (e) {
			console.log("le json n'a pas pu être décodé")
			parsedJson = {};
		}
		//console.log(parsedJson)
		if (parsedJson.message) {
			console.log(parsedJson.message)
			arreterRecherche()
			return(null)
		}
		idPlaces = traiterCreneaux(parsedJson)																//on utilise la fonction traiterCreneaux pour récupérer les places qui correspondent à nos critères
		if (idPlaces.length == 0){
			console.log("pas de places correspondant à la recherche, la recherche va être relancée...")
			recharger(e.tabId)																				//si aucune place de correspond, on recharge la page
		}
		else{
			console.log("demander au content script de réserver la place...")
			browser.tabs.sendMessage(e.tabId, {	
				action : "RESERVER",
				options : {														//on demande au content script de réserver la place ; on lui donne beaucoup d'informations inutiles, il n'a besoin que de idPlace et confirmation
					idPlace : idPlaces[0],
					confirmation : config.confirmation
				}
			})
		}
	})
}

function commencerRecherche(){
	console.log("démarrage de la recherhe dans le script d'arrière plan")
	rechercheEnCours = true
	browser.tabs.query({currentWindow: true, active: true}).then((tabs) => {
/*		if (!idTabsContentScript.includes(tabs[0].id)){
			browser.tabs.executeScript(tabs[0].id,{file: "/content-script.js"}).then(()=>{
				idTabsContentScript.push(tabs[0].id)
				browser.tabs.sendMessage(tabs[0].id, {action: "CHERCHER"})
			})
		}
		else{
			browser.tabs.sendMessage(tabs[0].id, {action: "CHERCHER"})
		}*/
		browser.tabs.sendMessage(tabs[0].id, {action: "CHERCHER"}).catch((e)=>{
			browser.tabs.executeScript(tabs[0].id, {file: "/content-script.js"}).then(()=>{
				idTabsContentScript.push(tabs[0].id)
				browser.tabs.sendMessage(tabs[0].id,{action: "CHERCHER"})
			}).catch((e)=>{console.log("problème avec le content script")})
		})
	})	// executer le contetn script dans l'onglet actif, puis lui demander de cliquer surle bouton "recherche" une première fois
	if (!browser.webRequest.onBeforeRequest.hasListener(listener)){
		browser.webRequest.onBeforeRequest.addListener(listener, {urls: ["https://candidat.permisdeconduire.gouv.fr/api/v1/candidat/creneaux?*"], types: ["xmlhttprequest"]}, ["blocking"]);					// on ajoute un listener: appelle la fonction "listener" à chaque fois qu'on fait une recherche de créneau
	}
}

function arreterRecherche(){
	console.log("arrêt de la recherche dans le script d'arrière plan")
	rechercheEnCours = false
	idTabsContentScript.forEach((id) => {browser.tabs.sendMessage(id, {action: "ARRETER"}).catch((e) => {console.log(id)})})																	// on demande au content script d'arrêter toutes les opérations en cours
	if(browser.webRequest.onBeforeRequest.hasListener(listener)){
		browser.webRequest.onBeforeRequest.removeListener(listener)																																		// on retire le listener	
	}
	clearTimeout(idTimeout)
}

function getRechercheEnCours(){
	return rechercheEnCours
}

function getConfig(){
	return config
}

function setConfig(nouvelleConfig){
	console.log("mise à jour des données de configuration dans le script d'arrière plan")
	config = nouvelleConfig
	config.isTest = false											// à modifier pour tromper le site avec des fausses données de créneaux, et faire des test de réservation
	config.testData = ''//[{"centre":{"id":"90e61d8a-a0f2-447e-b889-e081fbc89186","nom":"CHAMBON B","adresse":"2 rue RÃ©mi MoÃ¯se","complementAdresse":"","ville":"Chambon-Feugerolles","codePostal":"42500","codeDepartement":"042"},"creneaux":[{"id":"f31519b8-6e17-49c0-be52-cf3873432d0f","centreId":"90e61d8a-a0f2-447e-b889-e081fbc89186","dateDebut":"2024-07-15T13:30:00","dateFin":"2024-07-15T14:00:00","categoriePermis":"B"},{"id":"16da0184-4320-453b-baf3-576add8c7f1c","centreId":"90e61d8a-a0f2-447e-b889-e081fbc89186","dateDebut":"2024-07-28T13:30:00","dateFin":"2024-07-28T14:00:00","categoriePermis":"B"},{"id":"05c44c68-c84a-4fc1-850d-73685fdf8930","centreId":"90e61d8a-a0f2-447e-b889-e081fbc89186","dateDebut":"2024-07-11T13:30:00","dateFin":"2024-07-11T14:00:00","categoriePermis":"B"}]}]'
	console.log(config)
}

function setConfigFromStorage(){
	browser.storage.local.get().then(setConfig)
}

/*function updateConfig(changes, area){
	if (area == "local"){
		for (var item in Object.keys(changes)){
			config[item] = changes[item].newValue
		}
	}
	console
	* .log(config)
}

browser.storage.onChanged.addListener(updateConfig);*/

setConfigFromStorage()
