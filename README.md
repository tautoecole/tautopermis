# Tautopermis


tautopermis c'est un assistant pour réserver une place sur rdvPermis. C'est une extension pour le navigateur web Firefox.


## Historique


25/03/2024; on a obtenu trois dates de permis au cours des 2 derniers mois, le site ne semble pas avoir changé
09/01/2024; nouvelle version pour s'adapter aux changements du site web ; testé sans confirmation de la réservation (recherche lancée à 13h04, recherche toutes les 2 secondes, une place trouvée à 13h06 pour le 12 février)
02/10/2023; testé pour la dernière fois début septembre 2023


## Comment utiliser l'extension tautopermis?

c'est schlag... il faut l'installer comme extension temporaire

### Téléchargement

Sur la page https://0xacab.org/tautoecole/tautopermis cliquer sur le bouton bleu "code" sur la droite, puis "download source code" -> "zip"

Extraire l'archive zip

### Installation

sur firefox, taper dans la barre d'adresse : about:debugging

puis aller dans "ce firefox", puis "charger un module complémentaire temporaire...", et sélectionner n'importe quel fichier dans le dossier de l'extension. Par exemple : manifest.json

une icone de tautoécole devrait apparaitre en haut à droite...
parfois il faut d'abord cliquer sur un symbole "extension" en haut à droite, et l'icone de tautoecole apparait

### Paramètres

Quand on clique sur l'icone de tautoécole, une fenêtre apparait...

remplir ses informations, par exemple :

 - centre : CHAMBON B
 - délai : 0.5 (pour refaire la recherche toutes les 0.5 secondes)

L'information du centre d'examen n'est pas requise, si le champ est laissé vide, une place pourra être réservée dans n'importe quel centre d'examen. Si on ne connait pas le nom exact du centre d'examen, c'est peut etre mieux de laisser le champ vide.

ensuite, indiquer ses disponibilités sur les 2-3 prochains mois :

boutons "modifier mes dispos"

ajouter les mois qu'on veut en les selectionnant dans la liste

ensuite entrer les jours où on est disponible séparés par des virgules ; ou par des tirets du 6 pour indiquer une période, par exemple 1-31 si on est dispo du 1 au 31.

puis valider

si on veut réserver une place n'importe quel jour, on peut sauter cette étape et simplement cocher la case "ignorer mes dispos"

si on veut que l'extension aille jusqu'au bout et réserve effectivement la place, il faut cocher le bouton "confirmer la réservation". Si on ne coche pas la case, ça s'arretera juste avant la dernière étape. On peut toujours confirmer la réservation à la main mais c'est surement trop lent. C'est surtout utile de laisser la case décochée pour faire des tests.

### Choix du délai

Le délai, c'est le temps qu'on attend avant de relancer une recherche.

Au plus il est court, au plus on a une chance de trouver une place. Mais si il est trop court et qu'on fait trop de recherches, on risque d'être bloqué...

En réglant à 0.5, on est bloqué au bout d'environ 10 minutes.

Dans tous les cas, c'est mieux d'utiliser une connexion rapide (fibre etc..)

## Comment réserver une place?

### RDV Permis

pour commencer, il faut se connecter sur candidat.permisdeconduire.gouv.fr (si on a pas de compte, en créer un)

il faut avoir des informations relatives à la personnes qui va nous accompagner le jour de l'examen : nom, prénom, numéro NEPH, date d'obtention du permis de conduire (on trouve ça sur son permis de conduire normallement)

### Sans extension

cliquer sur "Réserver un examen", en haut à droite. On doit remplir les informations sur son accompagnateur.

Ensuite on arrive sur la page où on peut chercher des places d'examen.

On cherche une place dans son département, généralement, il n'y en a pas...

on peut recommencer la recherche, mais attention, il y a un nombre de recherches limité par jour

à saint etienne, on a interrogé des auto école et on a découvert que les places sortent le mardi et le jeudi entre 13h et 13h15, surtout le premier mardi du mois. On a quasiment toujours trouvé des places le premier mardi du mois. A priori, le jour est différent selon les départements, il vaut mieux trouver cette information pour son département si on veut avoir une chance de trouver une place.

### Avec extension

Pour rechercher une place avec l'extension tautopermis, une fois qu'on se trouve sur la page de recherche de places, remplir le champ "département" (par exemple pour saint-étienne : 042 - Loire)

Puis ouvrir le panneau de l'extension et cliquer sur "rechercher".

L'extension devrait cliquer sur le bouton de recherche jusqu'à trouver une place.

Si il y a une place qui correspond à nos dispo, elle devrait la réserver.

Ca va trop vite pour qu'on ait le temps de voir ce qu'il se passe, il faut aller voir ses mails pour connaître la date de l'examen.

## Attention

Ne pas aller directement sur la page de recherche de places : ça paraît plus sûr de passer par la page d'accueil de candidat.permisdeconduire.gouv.fr, et de remplir les infos sur son accompagnateurice avant d'arrier sur la page de recherche.

Les informations entrées dans l'extension sont automatiquement stockées de manière pas sécurisée, le temps qu'on garde firefox ouvert.

## Pour débugguer

### Ouvrir la console

Sur le site internet, appuyer sur les touches fn + F12, ou bien faire clic droit puis inspecter. Dans le panneau qui s'affiche, cliquer sur l'onglet "console".

Ici, on peut voir les messages et les erreurs du script qui s'exécute dans la page web. Il fait des choses comme cliquer sur des boutons, cocher des cases... On peut voir par exemple si il n'a pas trouvé un bouton.

### Ouvrir les devtools

Dans la page about:debugging -> ce firefox, où on a installé l'extension, cliquer sur "examiner" en haut à droite du cadre correspondan à l'extension.

Ici on voit les messages et les erreurs du script qui gère la fenêtre pop-up de l'extension, et du script d'arrière plan, qui fait tout le travail.

### Faire croire qu site qu'il y a des places disponibles pour faire des tests

Attention, si on fait ça, ne pas confirmer la réservation (ou peut être que ça marche? pas testé)

Ouvrir le fichier background-script.js dans un éditeur de texte

Modifier les lignes 135 et 136 :

config.isTest = true

config.testData = '[{"centre"...'

Entre guillemets simples ('), on met le fichier json qu'on veut que le site trouve quand il cherche des places. Un exemple de tel fichier est sur la même ligne en commentaire (après //). Il est utilisable tel quel mais attention, il vaut mieux modifier les dates pour qu'elles soient dans le futur

Enregistrer, actualiser l'extension et voila!
