let backgroundPage
const moisEntiers = {
	"01" : "Janvier",
	"02" : "Février",
	"03" : "Mars",
	"04" : "Avril",
	"05" : "Mai",
	"06" : "Juin",
	"07" : "Juillet",
	"08" : "Août",
	"09" : "Septembre",
	"10" : "Octobre",
	"11" : "Novembre",
	"12" : "Décembre"
}

function initialiser(valeurs){
	
	function remplirChamp(config){													//remplir les champs du formulaire avec les valeurs déjà enregistrées dans le stockage
		console.log(config)
		document.getElementById("centre").value = config.centre || ""
		document.getElementById("délai").value = parseFloat(config.delai || "1000")/1000
		document.getElementById("dispos").checked = config.dispos
		document.getElementById("confirmation").checked = config.confirmation
	}
	
	backgroundPage = valeurs[0]												//valeurs[0] : valeur de retour de la promesse gettingBackgroundPage
	console.log(backgroundPage)
	const storageItem = valeurs[1]											//valeurs[1] : valeur de retour de la promesse gettingStorage
	if (backgroundPage.getRechercheEnCours()){								//afficher un bouton rechercher ou arrêter, selon qu'il y a une recherche en cours ou pas
		document.querySelector(".rechercher").classList.add("hidden")
		document.querySelector(".arreter").classList.remove("hidden")
	}
//	const ancienneConfig = backgroundPage.getConfig();
//	remplirChamp(ancienneConfig)
	remplirChamp(storageItem)
	backgroundPage.setConfigFromStorage()
	if (backgroundPage.pageDateOuverte){
		ouvrirDates()
	}
}

function erreurInitialisation(e){
	console.log(e)
}

gettingBackgroundPage = browser.runtime.getBackgroundPage()
gettingStorage = browser.storage.local.get({								// variables qu'on récupère dans le storage, avec les valeurs par défaut
	centre : "",
	delai : 1000,
	dispos : false,
	confirmation : false
})


Promise.all([gettingBackgroundPage, gettingStorage]).then(initialiser, erreurInitialisation)	// initialiser après avoir récupérer la page de background d'une part, le contenu du stockage d'autre part


function modifChamp(e){			
	//console.log("modifChamp")								
	let delaiChamp = parseFloat(document.getElementById("délai").value)
	if (isNaN(delaiChamp)){
		afficherErreur("délai n'est pas un nombre")
		return false
	}
	let regexTexte = /^[^&~"#'{([|`_\\^@)\]°+=}€$¤µ*%?,.;/:§!><0-9]+$/
	let regexNumero = /^[0-9]{12}$/
	let regexDate = /^[0-9]{2}\/[0-9]{2}\/[0-9]{4}$/
	
	let centre = document.getElementById("centre").value
	let confirmation = document.getElementById("confirmation").checked
	let dispos = document.getElementById("dispos").checked
	//console.log(dispos)
	if (!regexTexte.test(centre)){
		afficherErreur("caractère spéciaux non autorisés")
	}
	else {
		cacherErreur()
	}
	browser.storage.local.set({
		centre : centre,
		delai : delaiChamp*1000,
		dispos : dispos,
		confirmation : confirmation
	}).then(backgroundPage.setConfigFromStorage)
}

document.querySelectorAll("input").forEach( (elementInput) => {elementInput.addEventListener("change", modifChamp)})

function afficherErreur(texteErreur){
	console.log(texteErreur)
	document.getElementById("texte-erreur").innerHTML = texteErreur
	document.getElementById("error-content").classList.remove("hidden")
}

function cacherErreur(){
	document.getElementById("error-content").classList.add("hidden")
}

function ajouterElementMois(y, m, j){									// pour ajouter une ligne de disponibilité correspondant à un mois, dans la page de sélection des disponibilité
//	console.log("ajouter mois : " + m + "" + y)
	const sectionDate = document.getElementById("sectionDate")
	if (!sectionDate.querySelector('.d[mois="'+m+'"][annee="'+y+'"]')){	// on vérifie que la ligne correspondant à ce mois nest pas déjà présente
		let labelMois = document.createElement("label")
		labelMois.innerHTML = moisEntiers[m] + " " + y + " : "
		labelMois.classList.add("labelMois")							//créer un label, avec du texte (ex : Janvier 2024 : ), et la classe labelMois
		let inputMois = document.createElement("input")
		inputMois.classList.add("d")
		inputMois.setAttribute("mois", m)
		inputMois.setAttribute("annee", y)								//créer un input de type text, avec la classe d et les attributs mois et annee correspondant
		let jours = ""
		for (const i in j){
			jours = jours + j[i]
			if (i< j.length-1){
				jours = jours+","
			}
		}
		inputMois.value = jours											//remplir l'input créer avec les jours déjà déclarés comme disponible (aucun si on crée le champ ; ceux du stockage si on récupère dans le stockage
		inputMois.addEventListener("change", modifChampDate)
		labelMois.appendChild(inputMois)								// on veut que le input soit nested dans le label
		sectionDate.appendChild(labelMois)
	}
	else{
		console.log("mois déjà présent")
	}
}

function traiterChaineMois(chaine){										//pour transformer la chaine de caractère saisie dans un champ dans la page des disponibilité, en une liste de jours
	let l = chaine.split(",")											// découper selon les virgules
	let l1 = l.flatMap( (str)=> {
		let i = str.indexOf("-")										// si il y a un tiret
		if (i==-1){
			return str
		}
		else{
			let debut = parseInt(str.substring(0,i))
			let fin = parseInt(str.substring(i+1))
			if (isNaN(debut) || isNaN(fin)){
				return "NaN"
			}
			else {
				return Array.from(Array(fin-debut+1), (x,j) => (j+debut).toString())	// on ajoute toutes les valeurs entre la valeur avant et après le tiret
			}
		}
	})
	let l2 = l1.map( (jour) => {
		jour = jour.trim()
		if (jour.length == 1){
			return "0" + jour
		}
		else{
			return jour
		}
	})
	let l3 = l2.filter( (jour) => {										// vérifier que chaque élément est un nombre compris entre 1 et 31
		let jourNombre = parseInt(jour)
		return (!isNaN(jourNombre) && jourNombre >= 1 && jourNombre <= 31)
	})
	return l3
}

function ouvrirDates(){																	//bouton modifier mes dispos : on remplace le contenu de la popup par un écran pour saisir ses disponibilités. Si besoin, on y ajoute des champs pour tous les mois contenus dans le stockage local
	cacherErreur()
	document.getElementById("popup-content").classList.add("hidden")
	document.getElementById("date-content").classList.remove("hidden")
	const sectionDate = document.getElementById("sectionDate")
	browser.storage.local.get({date : {"2023" : {}}}).then( (dateStorage) => {
		for (const year in dateStorage.date){
			for (const month in dateStorage.date[year]){
				ajouterElementMois(year, month, dateStorage.date[year][month])
			}
		}						
					
	})
	backgroundPage.pageDateOuverte = true
}

function modifChampDate(e){
	//console.log("modif champ date")
	const listeInputMois = document.querySelectorAll("input.d")
	//console.log(listeInputMois)
	browser.storage.local.get({date : {}}).then( (dateStorage) => {							//on commence par récupérer l'objet date dans le stockage, pour pouvoir ensuite le modifier
		let dateSaisie = dateStorage.date ? dateStorage.date : {}
		for (let i of listeInputMois){
			const mois = i.getAttribute("mois")
			const annee = i.getAttribute("annee")
			if (!dateSaisie[annee]){
				dateSaisie[annee] = {}
			}
			dateSaisie[annee][mois] = traiterChaineMois(i.value)
		}
		return browser.storage.local.set({date : dateSaisie})							//enregistrer l'objet modifié
	}).then(backgroundPage.setConfigFromStorage)										//et mettre à jour la config dans le script d'arrière plan
}
	

document.addEventListener("click", (e)=> {								// réagir aux clics sur des éléments de la popup
	
//	console.log(e);
	
	function rechercher() {												//bouton rechercher : on affiche le bouton arrêter à la place du bouton arrêter, et on lance la recherce dans le bakground script
		document.querySelector(".rechercher").classList.add("hidden")
		document.querySelector(".arreter").classList.remove("hidden")
		backgroundPage.commencerRecherche()
	}
	
	function arreter() {												//bouton arrêter : l'inverse
		document.querySelector(".arreter").classList.add("hidden")
		document.querySelector(".rechercher").classList.remove("hidden")
		backgroundPage.arreterRecherche()
	}
	
	function ajouterMois(){																	// dans l'écran des disponibilités, ajouter un mois de disponiblité défini par le contenu des éléments moisAjoute et anneeAjoute
/*		const moisAAjouter = document.getElementById("moisAjoute").value
		if (moisAAjouter.length == 7){
			var mois = moisAAjouter.substring(5,7)
			var annee = moisAAjouter.substring(0,4)
		}*/
		const mois = document.getElementById("moisAjoute").value
		const annee = document.getElementById("anneeAjoute").value
		ajouterElementMois(annee, mois, Array.from(Array(31), (x,j) => (j+1).toString()))
	}
	
	function fermerDates(){																	// fermer l'écran de choix des disponiblités, en enregistrant dans le stockage local les valeurs saisies
		document.getElementById("date-content").classList.add("hidden")
		document.getElementById("popup-content").classList.remove("hidden")		
		backgroundPage.pageDateOuverte = false
	}
	
	if (e.target.classList.contains("rechercher")){
		rechercher()
	}
	if (e.target.classList.contains("arreter")){
		arreter()
	}
//	if (e.target.classList.contains("sauvegarder")){
//		sauvegarder()
//	}
	if (e.target.classList.contains("dates")){
		ouvrirDates()
	}
	if (e.target.classList.contains("valider")){
		fermerDates()
	}
	if (e.target.classList.contains("ajouterMois")){
		ajouterMois()
	}
});
