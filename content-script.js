(()=>{


var enCours = false												// variable pour savoir si on est en train de faire des trucs : elle passe à true quand on entre dans une fonction, elle repasse à false quand on sort de la fonction. On peut la forcer à false, ça fait sortir de la fonction. Pour éviter de faire plusieurs choses en même temps.

function reserverPlace(m){										// faire toutes les actions nécessaires pour réserver une place
	enCours = true
	
	console.log(m)
	
	let etape = 1												// c'est diviser en étape qu'on doit faire les unes à la suite des ordres, dans cette variable on stocke le nméro de la prochaine étape à faire
	
	
	function etapeCreneau(){									// étape 1 : cliquer sur l'horaire qui nous intéresse (on sélectionne en fonction de l'id)
		const boutonCreneau	= document.getElementById(m.idPlace)
		if (boutonCreneau){
			etape++
			boutonCreneau.click()
		}
		else{
			console.log("bouton créneau pas trouvé...")
		}
	}
	
	function etapeSuivante(){									// étape 2 : cliquer sur le bouton "etape suivante"
		let buttons = document.querySelectorAll('button')
		console.log(buttons)
		let button = buttons[buttons.length-1]					// on sélectionne le dernier élément button, et on vérifie qu'il y a bien écrit "étape suivante" dessus ; surement pas la méthode la plus rapide
		if (button && button.innerText == "Étape suivante"){
			button.click()
			etape++
		}
		else{
			console.log("bouton étape suivante pas trouvé...")
		}
	}
	
	function etapeCasesACocher(){								// étape 3 : cocher les deux cases blablabla
		const boutonAccompagnateur = document.getElementById("confirmation-accompagnateur-present-checkboxes-input")
		const boutonVehicule = document.getElementById("confirmation-vehicule-double-commande-checkboxes-input")
		console.log(boutonAccompagnateur)
		console.log(boutonVehicule)
		if (boutonAccompagnateur && boutonVehicule){
			boutonAccompagnateur.click()
			boutonVehicule.click()
			etape++
		}
		else{
			console.log("bouton confirmation accompagnateur ou vehicule pas trouvé...")
		}
	}
	
	
	function etapeConfirmationReservation(){				// étape 4 : cliquer sur le bouton "Confirmer la réservation"
		let buttons = document.querySelectorAll('button')
		console.log(buttons)
		let button = buttons[buttons.length-1]				// pareil, on sélectionne le dernier button et on vérifie son texte, et si il est bien activé
		if (button && button.innerText == "Confirmer la réservation" && !button.disabled){
			if (m.confirmation){							// on ne clique que si l'option "confirmer" de l'extension a été cochée
				button.click()
			}
			else{
				console.log("confirmation pas cliqué...")
			}
			etape++
			observer.disconnect()
			enCours = false
		}
		else{
			console.log("bouton confirmation réservation pas trouvé...")
		}
	}
	
//	input id="confirmation-accompagnateur-present-checkboxes-input"
	
//	input id="confirmation-vehicule-double-commande-checkboxes-input"
	
//	dernier bouton (text : "Confirmer la réservation")
	
	
		
	
	const callback = (mutationList, observer) => {			// quand l'observateur de mutation observe des changements dans la page
		console.log("Etape " + etape)
		if (!enCours){										// si enCours = false, on arrête tout
			observer.disconnect()
			console.log("actions en cours interrompues")
			return false
		}									
		switch (etape){										
			case 1:
			etapeCreneau();
			break;
			case 2:
			etapeSuivante();
			break;
			case 3:
			etapeCasesACocher();
			break;
			case 4:
			etapeConfirmationReservation();
			break;
		}

	}
	const observer = new MutationObserver(callback);
	
	observer.observe(document.getElementById("root"), { attributes: true, childList: true, subtree: true }); // on observe tous les changements d'attributs, les ajouts et retraits d'éléments enfants, dans l'élément root et tous ses descendants
	callback([], observer)								// lancer la fonction callback une première fois
};

/*function chercher(){						//faire toutes les actions nécessaires pour rechercher des places. Cette fonction n'est plus appelée car il n'y a plus de popup
	let etape = 1
	
	enCours = true
	
	function fermerAlerte(){				// étape 1 : cliquer sur la croix pour fermer la popup qui dit "pas de places disponibles" 
		const boutonCroix = document.querySelector("button[aria-label=\"Fermer l'alerte\"")
		console.log(boutonCroix)
		if(boutonCroix){
			boutonCroix.click()
			etape++
		}
		else{
			cliquerBoutonChercher()	//si pas de bouton croix,probablment, parce qu'il n'y a pas de popup à fermer: on saute cette étape
		}
	}
	
	function cliquerBoutonChercher(){		// étape 2 : cliquer sur le bouton "Rechercher"
		const boutonChercher = document.querySelector('form button')	// on repère un élément button descendant d'un élément form
		console.log(boutonChercher)
		if(boutonChercher){
			boutonChercher.click()
			etape++
			observerChercher.disconnect()
			enCours = false
		}
	}
	
	const callback = (mutationList, observerChercher) => {			// quand l'observateur de mutation observe des changements dans la page
		console.log(etape)
		if (!enCours){												// si enCours = false, on arrête tout
			observerChercher.disconnect()
			console.log("actions en cours interrompues")
			return false
		}									
		switch (etape){										
			case 1:
			fermerAlerte();
			break;
			case 2:
			cliquerBoutonChercher();
			break;
		}

	}
	const observerChercher = new MutationObserver(callback);
	
	observerChercher.observe(document.getElementById("root"), { attributes: true, childList: true, subtree: true }); // on observe tous les changements d'attributs, les ajouts et retraits d'éléments enfants, dans l'élément root et tous ses descendants
	callback([], observerChercher)
}*/

function chercherSansPopup(){
	const boutonChercher = document.querySelector('form button')	// on repère un élément button descendant d'un élément form
	console.log(boutonChercher)
	boutonChercher.click()
}

console.log("content script loaded")

browser.runtime.onMessage.addListener((m) => {
	if (m.action == "RESERVER"){						//action RESERVER : on lance la fonction réserverPlace
		if (!enCours){
			reserverPlace(m.options)
		}
	}
	if (m.action == "CHERCHER"){						//action CHERCHER : on lance la fonction chercher
		if (!enCours){
			chercherSansPopup()
		}
	}
	if (m.action == "ARRETER"){							//action ARRETER : on modifie la valeur de la variable enCours pour arrêter les opérations en cours.
		console.log("arreter...")
		enCours = false
	}
})

})();

